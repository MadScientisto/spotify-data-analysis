# Spotify Data Analysis

A simple Data Analysis project on spotify music data using R. 

The goal of the project is to identify music trends across time, provide interesting visualizations and derive useful conclusions about audiences and the music industry.

View in .html format [here](https://madscientisto.gitlab.io/spotify-data-analysis).

Data Source [here](https://developer.spotify.com/documentation/web-api/reference/#/operations/get-several-audio-features).


